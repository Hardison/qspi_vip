
interface axi_lite_interface(input logic aclk_i,areset_n_i);
  
  //  // write Interface Signals
  logic [31:0] write_addr_i;
  logic        write_addrv_i;
  logic [31:0] write_data_i;
  logic       write_datav_i;
  logic [3:0] write_strb_i;
  logic [1:0] write_bresp_o;
  logic       write_bvalid_o;
  // read Interface Signals
  logic         read_start_i;
  logic [31:0] read_addr_i;
  logic        read_af_i;
  logic [31:0] read_data_o;
  logic        read_datav_o;
  
  
/*    
  //---------------------------------------
  //driver clocking block
  //---------------------------------------
  clocking driver_cb @(posedge clk);
    default input #1 output #1;
    output addr;
    output wr_en;
    output rd_en;
    output wdata;
    input  rdata;  
  endclocking
  
  //---------------------------------------
  //monitor clocking block
  //---------------------------------------
  clocking monitor_cb @(posedge clk);
    default input #1 output #1;
    input addr;
    input wr_en;
    input rd_en;
    input wdata;
    input rdata;  
  endclocking
  
  //---------------------------------------
  //driver modport
  //---------------------------------------
  modport DRIVER  (clocking driver_cb,input clk,reset);
  
  //---------------------------------------
  //monitor modport  
  //---------------------------------------
  modport MONITOR (clocking monitor_cb,input clk,reset);
  */
endinterface