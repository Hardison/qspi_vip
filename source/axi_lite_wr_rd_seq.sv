class axi_lite_wr_rd_seq extends uvm_sequence#(axi_lite_seq_item);
  
  `uvm_object_utils(axi_lite_wr_rd_seq)
   
  //--------------------------------------- 
  //Constructor
  //---------------------------------------
  function new(string name = "axi_lite_wr_rd_seq");
    super.new(name);
  endfunction
  
  bit [31:0] waddr;
  bit [128:0] wdata;
  bit  addrv_i;
  
  //bit [3:0] length;
  bit [31:0] raddr;
  bit [3:0] arid;  
    
  virtual task body();
    
    req = axi_lite_seq_item::type_id::create("req");
    wait_for_grant();
    req.randomize();
    
    //req.write_start_i     = 1'b1;
    req.write_addr_i      = this.waddr; 
    req.write_addrv_i     = this.addrv_i;
    //req.write_addrv_i     = 1'b1;
    req.write_data_i     = this.wdata;
    req.write_datav_i     = 1'b1;
    req.write_strb_i      = 4'b1111;
    
    send_request(req);
    wait_for_item_done();
    
    //read
    req = axi_lite_seq_item::type_id::create("req");
    wait_for_grant();
    req.randomize();
    
    req.read_start_i     = 1'b1;
    req.read_addr_i      = this.raddr;
    req.read_af_i        = 1'b0;                      
                        
    send_request(req);
    wait_for_item_done();
    
  endtask : body
  
endclass :axi_lite_wr_rd_seq
