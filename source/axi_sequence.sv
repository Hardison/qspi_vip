
class axi_sequence extends uvm_sequence#(axi_seq_item);
 
  `uvm_object_utils(axi_sequence)
  
  //--------------------------------------- 
  //Constructor
  //---------------------------------------
  function new(string name = "axi_sequence");
    super.new(name);
  endfunction
   
  `uvm_declare_p_sequencer(axi_sequencer)
  
  //---------------------------------------
  // create, randomize and send the item to driver
  //---------------------------------------
  virtual task body();
    repeat(3) 
      begin        
       req = axi_seq_item::type_id::create("req");
       wait_for_grant();
       req.randomize();
       send_request(req);
       wait_for_item_done();
      end
  endtask 
  
endclass


