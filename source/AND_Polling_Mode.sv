// Reading the flash status register in polling mode.
`include "defines.sv"

class AND_Polling_Mode extends axi_lite_base_test;
  
  `uvm_component_utils(AND_Polling_Mode)
  
  //---------------------------------------
  // sequence instance 
  ////--------------------------------------- 
 
  axi_lite_write_seq wseq;
  axi_lite_read_seq rseq;

  //---------------------------------------
  // constructor
  //---------------------------------------
  function new(string name = "AND_Polling_Mode",uvm_component parent=null);
    super.new(name,parent);
  endfunction : new

  //---------------------------------------
  // build_phase
  //---------------------------------------
  virtual function void build_phase(uvm_phase phase);
    super.build_phase(phase);

    // Create the sequence
    
    wseq = axi_lite_write_seq::type_id::create("wseq");
    rseq = axi_lite_read_seq::type_id::create("rseq");
		
  endfunction : build_phase
  
  //---------------------------------------
  // run_phase - starting the test
  //---------------------------------------
 
	task run_phase(uvm_phase phase);
    
    phase.raise_objection(this);    
	wseq.waddr      = 0; 
	wseq.wdata      = 32'h00000000;
	wseq.addrv_i    = 1'b0;
	wseq.datav_i    = 1'b0;
	
	#100000;
	wseq.addrv_i    = 1'b1;
	wseq.datav_i    = 1'b1;
	    
	// sending address and data to QUADSPI_CR
	wseq.waddr   = `QUADSPI_CR; 
	wseq.wdata   = 32'h034f0f01;	 // AND match mode, APMS=1 Flash  25 MHz 
    wseq.start(env.axi_agnt.sequencer);
	
    // sending data to QUADSPI_DCR
	wseq.waddr   = `QUADSPI_DCR; 
	wseq.wdata   = 32'h001b0001;  
    wseq.start(env.axi_agnt.sequencer);
        
    wait(axi_master_tb_top_qspi.flash_inst.flash.Chip_EN);
  
	
	
	///////////////WEL///////////
  // sending data to QUADSPI_CCR
	wseq.waddr   = `QUADSPI_CCR;	
	wseq.wdata   = 32'h00000106;  //  WEL
    wseq.start(env.axi_agnt.sequencer);

	wait(axi_master_tb_top_qspi.qspi_inst.qspi_ifc_rg_sr_tcf);  	
	// sending data to QUADSPI_FCR
	wseq.waddr   = `QUADSPI_FCR; 
	wseq.wdata   = 32'h0000001b; 
    wseq.start(env.axi_agnt.sequencer);
	
	//---------------------------------------
	// Reading from Flash Started
	//---------------------------------------	

//////////////Polling Interval register/////////////
	// setting data to QUADSPI_PIR
	wseq.waddr   = `QUADSPI_PIR; 
	wseq.wdata   = 32'h0000000f; // 3 clk cycles interval 
    wseq.start(env.axi_agnt.sequencer);
	
	//////////////PS Mask register/////////////
	// setting data to QUADSPI_PSMKR
	wseq.waddr   = `QUADSPI_PSMKR; 
	wseq.wdata   = 32'hFFFFFFFF; // all bits are unmasked
    wseq.start(env.axi_agnt.sequencer);
	
	#10000000;
   // setting data to QUADSPI_PSMAR
	wseq.waddr   = `QUADSPI_PSMAR; 
	wseq.wdata   = 32'h00000002; //matching WEL bit value
    wseq.start(env.axi_agnt.sequencer);

	
			
	// sending data to QUADSPI_DLR
	wseq.waddr   = `QUADSPI_DLR; 
	wseq.wdata   = 32'h00000000; //  Reading 1 bytes data
    wseq.start(env.axi_agnt.sequencer);

	// sending data to QUADSPI_CCR
	wseq.waddr   = `QUADSPI_CCR;
	wseq.wdata   =  32'h09002505; // Reading flash status Register.
    wseq.start(env.axi_agnt.sequencer);	
	     		
     // sending data to QUADSPI_AR
	wseq.waddr   = `QUADSPI_AR;  
	wseq.wdata   = 32'h00000000;
    wseq.start(env.axi_agnt.sequencer);	 
	 
#50000000; //50us
// Reading data from QUADSPI_DR
	rseq.raddr = `QUADSPI_DR;
    rseq.start(env.axi_agnt.sequencer);

	#20000000;
    wait(axi_master_tb_top_qspi.qspi_inst.qspi_ifc_rg_sr_tcf);  	
	// sending data to QUADSPI_FCR
	wseq.waddr   = `QUADSPI_FCR; 
	wseq.wdata   = 32'h0000001b; 
    wseq.start(env.axi_agnt.sequencer);
  
  $display("AND_Polling_Mode command test case completed");
	
	phase.drop_objection(this);
	
    //set a drain-time for the environment if desired
	phase.phase_done.set_drain_time(this, 100);
  endtask : run_phase

endclass : AND_Polling_Mode