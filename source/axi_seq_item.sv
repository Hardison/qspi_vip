

class axi_seq_item extends uvm_sequence_item;
  //---------------------------------------
  //data and control fields
  //---------------------------------------
 /*
  // Write Address Channel Signals
  bit [3:0]    awid_o;
  bit [31:0]   awaddr_o;
  bit [7:0]    awlen_o;
  bit [2:0]    awsize_o;
  bit [1:0]    awburst_o;
  bit [1:0]    awlock_o;
  bit          awvalid_o;
  bit          awready_i;
  // Write Data Channel Signals
  bit [3:0]    wid_o;
  bit [63:0]  wdata_o;
  bit [7:0]   wstrb_o;
  bit          wlast_o;
  bit          wvalid_o;
  bit          wready_i;
  // Write Response Channel Signals
  bit [3:0]    bid_i;
  bit [1:0]    bresp_i;
  bit          bvalid_i;
  bit          bready_o;
  // Read Address Signals
  bit [3:0]    arid_o;
  bit [31:0]   araddr_o;
  bit [7:0]    arlen_o;
  bit [2:0]    arsize_o;
  bit [1:0]    arburst_o;
  bit [1:0]    arlock_o;
  bit          arvalid_o;
  bit          arready_i;
  // Read Data Channel Signals
  bit [3:0]    rid_i;
  bit [63:0]  rdata_i;
  bit [1:0]    rresp_i;
  bit          rlast_i;
  bit          rvalid_i;
  bit          rready_o;*/
  // write Interface Signals
  rand bit           write_start_i;
  rand bit [3:0]     write_awid_i;
  rand bit [31:0]    write_addr_i;
  rand bit [7:0]     write_length_i;
  rand bit [2:0]     write_size_i;
  rand bit [1:0]     write_burst_i;
  rand bit [1:0]     write_lock_i;
  rand bit [3:0]     write_wid_i;
  rand bit [63:0]    write_data_i;
  rand bit           write_datav_i;
  rand bit [15:0]    write_strb_i;
  rand bit           write_ack_o;
  bit                write_data_req_o;
  bit                write_done_o;
  bit                write_err_o;
  bit  [1:0]         write_bresp_o;
  bit  [3:0]         write_bid_o;
  bit                write_bvalid_o;
  // read Interface Signals
   bit               read_start_i;
   bit [3:0]         read_arid_i;
   bit [31:0]        read_addr_i;
   bit [7:0]         read_length_i;
   bit [2:0]         read_size_i;
   bit [1:0]         read_burst_i;
   bit [1:0]         read_lock_i;
   bit               read_af_i;
  bit                read_ack_o;
  bit [63:0]         read_data_o;
  bit                read_datav_o;
  bit                read_err_o;
  bit                read_done_o;
  
  //slave output signals
  // write signals
 /* bit            awready_o;
  bit            wready_o;
  bit            bid_o;
  bit[1:0]       bresp_o;
  bit            bvalid_o;
  //read signals
  
  bit          arready_o;
  bit [3:0]    rid_o;
  bit [63:0]  rdata_o;
  bit [1:0]    rresp_o;
  bit          rlast_o;
  bit          rvalid_o;*/
  
  
  `uvm_object_utils(axi_seq_item)
  
  //---------------------------------------
  //Constructor
  //---------------------------------------
  
  function new(string name = "axi_seq_item");
    super.new(name);
  endfunction
  
  //---------------------------------------
  //constaint, to generate any one among write and read
  //---------------------------------------
  //constraint wr_rd_c{ write_start_i != read_start_i;};

endclass : axi_seq_item