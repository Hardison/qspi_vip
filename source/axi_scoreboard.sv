
class axi_scoreboard extends uvm_scoreboard;
  
  //---------------------------------------
  // declaring pkt_qu to store the pkt's recived from monitor
  //---------------------------------------
  axi_seq_item pkt_qu[$];
  
  //axi_seq_item data_qu[$];
  
  
  //---------------------------------------
  // sc_axi 
  //---------------------------------------
  //bit aclk_i;
  
  //Slave signals
  bit   wready = 1;
  bit [127:0] slave_data_out;
  
  
  //---------------------------------------
  //port to recive packets from monitor
  //---------------------------------------
  uvm_analysis_imp#(axi_seq_item, axi_scoreboard) item_collected_export;
  `uvm_component_utils(axi_scoreboard)

  //---------------------------------------
  // new - constructor
  //---------------------------------------
  function new (string name, uvm_component parent);
    super.new(name, parent);
  endfunction : new
  //---------------------------------------
  // build_phase - create port and initialize local memory
  //---------------------------------------
  function void build_phase(uvm_phase phase);
    super.build_phase(phase);
    item_collected_export = new("item_collected_export", this);
  endfunction: build_phase
  
  //---------------------------------------
  // write task - recives the pkt from monitor and pushes into queue
  //---------------------------------------
  virtual function void write(axi_seq_item pkt);
    //pkt.print();
    pkt_qu.push_back(pkt);
  endfunction : write

  //---------------------------------------
  // run_phase - compare's the read data with the expected data
  //---------------------------------------
  virtual task run_phase(uvm_phase phase);
    axi_seq_item axi_pkt;
    //axi_seq_item axi_pkt[$];
    //axi_pkt = new();
    forever begin
      wait(pkt_qu.size() > 0);
      axi_pkt = pkt_qu.pop_front();
      //$display("scb...axi_pkt.write_length_i= %h",axi_pkt.write_length_i);
      // $display("scb...axi_pkt.write_data_i= %h",axi_pkt.write_data_i);
      
      //wait(axi_pkt.write_start_i) begin
        //$display("scb...axi_pkt.write_start_i= %h",axi_pkt.write_start_i);
      //end
      /*if(axi_pkt.write_datav_i == 1'b1) begin
      $display("scb...axi_pkt.write_data_i= %h",axi_pkt.write_data_i);
      end
      else;*/
      
      if((axi_pkt.write_datav_i == 1'b1) && (wready == 1'b1)) begin
        $display("%d scoreboard checking",axi_pkt.write_datav_i );   
        if(axi_pkt.write_length_i == 0) begin
          //$display("axi_pkt.write_length_i = %d",axi_pkt.write_length_i );              
          this.slave_data_out  = axi_pkt.write_data_i;
          #10;
        end
        else
          //$display("else part");
          for(int i = 0; i<=axi_pkt.write_length_i ; i++) begin
            $display("axi_pkt.write_length_i = %d",axi_pkt.write_length_i ); 
            this.slave_data_out  = axi_pkt.write_data_i;
            //this.slave_data_out  = data_qu.wdata_o;
            #10;
            $display("scb...axi_pkt.write_data_i= %h",axi_pkt.write_data_i);
            //$display("scb...axi_pkt.read_data_o= %h",axi_pkt.read_data_o);
          end
      end
      if(this.slave_data_out == axi_pkt.read_data_o) begin
        //$display("scb...this.slave_data_out= %h",this.slave_data_out);
        //$display("scb...axi_pkt.read_data_o= %h",axi_pkt.read_data_o);
        $display(" test passed");
      end
      else
        $display(" test failed");
    end
  endtask : run_phase
endclass : axi_scoreboard