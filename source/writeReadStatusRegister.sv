// writing to flash status register with 01h command but unable to set QE bit in Flash(status_reg[6]).


`include "defines.sv"
class writeReadStatusRegister extends axi_lite_base_test;
  
  `uvm_component_utils(writeReadStatusRegister)
  
  //---------------------------------------
  // sequence instance 
  ////--------------------------------------- 
  //wr_rd_sequence seq;
  
  axi_lite_write_seq wseq;
  axi_lite_read_seq rseq;

  //---------------------------------------
  // constructor
  //---------------------------------------
  function new(string name = "writeReadStatusRegister",uvm_component parent=null);
    super.new(name,parent);
  endfunction : new

  //---------------------------------------
  // build_phase
  //---------------------------------------
  virtual function void build_phase(uvm_phase phase);
    super.build_phase(phase);

    // Create the sequence
    
    wseq = axi_lite_write_seq::type_id::create("wseq");
    rseq = axi_lite_read_seq::type_id::create("rseq");
		
  endfunction : build_phase
  
  //---------------------------------------
  // run_phase - starting the test
  //---------------------------------------
  task run_phase(uvm_phase phase);
    
    phase.raise_objection(this); 
	
	wseq.waddr      = 0; 
	wseq.wdata      = 32'h00000000;
	wseq.addrv_i    = 1'b0;
	wseq.datav_i    = 1'b0;
	
	#100000;
	wseq.addrv_i    = 1'b1;
	wseq.datav_i    = 1'b1;
	    
	// sending address and data to QUADSPI_CR
	wseq.waddr   = `QUADSPI_CR; 
	wseq.wdata   = 32'h031f0f01; // Flash  25 MHz
    wseq.start(env.axi_agnt.sequencer);
	   
	// sending data to QUADSPI_DCR
	wseq.waddr   = `QUADSPI_DCR; 
	wseq.wdata   = 32'h001b0001; 
    wseq.start(env.axi_agnt.sequencer);
	  
    wait(axi_master_tb_top_qspi.flash_inst.flash.Chip_EN);
	
	// sending data to QUADSPI_CCR
	wseq.waddr   = `QUADSPI_CCR;
	//wseq.wdata   = 32'h00003106;  //  
	wseq.wdata   = 32'h00000106;  //  
	 
    wseq.start(env.axi_agnt.sequencer);

	wait(axi_master_tb_top_qspi.qspi_inst.qspi_ifc_rg_sr_tcf);
	   	
	// sending data to QUADSPI_FCR
	wseq.waddr   = `QUADSPI_FCR; 
	wseq.wdata   = 32'h0000001b; 	 
    wseq.start(env.axi_agnt.sequencer);

    wait(!(axi_master_tb_top_qspi.flash_inst.flash.WIP)); 
	
	//---------------------------------------
	// Writing to Status register with 01 command
	//---------------------------------------	
	// sending data to QUADSPI_DLR
	wseq.waddr   = `QUADSPI_DLR;
	wseq.wdata   = 32'h00000000;  //  	 
    wseq.start(env.axi_agnt.sequencer); 
	
	// sending data to QUADSPI_CCR
	wseq.waddr   = `QUADSPI_CCR;
	//wseq.wdata   = 32'h01000101;  // Writing data to the Flash status register, but unable to do. 
	wseq.wdata   = 32'h01003D01;  // Writing data to the Flash status register, but unable to do. 	 
    wseq.start(env.axi_agnt.sequencer);
		
	// sending data to QUADSPI_AR
	wseq.waddr   = `QUADSPI_AR;  
	//wseq.wdata   = 32'h00010000;
	wseq.wdata   = 32'h00000000;	 
    wseq.start(env.axi_agnt.sequencer);					
					
	// sending data to QUADSPI_DR 1
	wseq.waddr   = `QUADSPI_DR;
	wseq.wdata   = 32'h40000000;  // This is data we are writing in flash.	 
    wseq.start(env.axi_agnt.sequencer);				
	
	wait(axi_master_tb_top_qspi.qspi_inst.qspi_ifc_rg_sr_tcf);
		  
	// sending data to QUADSPI_FCR
	wseq.waddr   = `QUADSPI_FCR; 
	wseq.wdata   = 32'h0000001b; // Flag Clear Register	 
    wseq.start(env.axi_agnt.sequencer);			  

	wait(!(axi_master_tb_top_qspi.flash_inst.flash.WIP)); 	
	
	//---------------------------------------
	// Reading from Flash Started
	//---------------------------------------		
			
	// sending data to QUADSPI_DLR
	wseq.waddr   = `QUADSPI_DLR; 
	wseq.wdata   = 32'h00000000; //  Reading 1 bytes data	 
    wseq.start(env.axi_agnt.sequencer);

	// sending data to QUADSPI_CCR
	wseq.waddr   = `QUADSPI_CCR;
	//wseq.wdata   =  32'h05200105; // Reading flash status Register,We are getting default values even though we have written in the Flash Status Register. 
	//wseq.wdata   =  32'h07180305; // Reading flash status Register.6 dummy Quad mode
	wseq.wdata   =  32'h05000105; // Reading flash status Register.6 dummy Quad mode	 
    wseq.start(env.axi_agnt.sequencer);		  
	
    #10000000;	
    // Reading data from QUADSPI_DR
	rseq.raddr = `QUADSPI_DR;
    rseq.start(env.axi_agnt.sequencer);      
	
    wait(axi_master_tb_top_qspi.qspi_inst.qspi_ifc_rg_sr_tcf);
	    		
	// sending data to QUADSPI_FCR
	wseq.waddr   = `QUADSPI_FCR; 
	wseq.wdata   = 32'h0000001b; // Flag Clear Register	 
    wseq.start(env.axi_agnt.sequencer);
	
/*  wait(!(axi_master_tb_top_qspi.flash_inst.flash.WIP));
	
 	// sending data to QUADSPI_CCR
	wseq.waddr   = `QUADSPI_CCR;    
	wseq.wdata   = 32'h00000135;    	 
    wseq.start(env.axi_agnt.sequencer);
    
    wait(axi_master_tb_top_qspi.qspi_inst.qspi_ifc_rg_sr_tcf);
    // sending data to QUADSPI_FCR
	wseq.waddr   = `QUADSPI_FCR; 
	wseq.wdata   = 32'h0000001b; 	 
    wseq.start(env.axi_agnt.sequencer);	 */
    
    wait(!(axi_master_tb_top_qspi.flash_inst.flash.WIP));
	
	// sending data to QUADSPI_CCR
	wseq.waddr   = `QUADSPI_CCR;
	//wseq.wdata   = 32'h00000106;     
	wseq.wdata   = 32'h00000306; 	 
    wseq.start(env.axi_agnt.sequencer);
    
    wait(axi_master_tb_top_qspi.qspi_inst.qspi_ifc_rg_sr_tcf);
    // sending data to QUADSPI_FCR
	wseq.waddr   = `QUADSPI_FCR; 
	wseq.wdata   = 32'h0000001b; 	 
    wseq.start(env.axi_agnt.sequencer);	
    
    wait(!(axi_master_tb_top_qspi.flash_inst.flash.WIP)); 

    // sending data to QUADSPI_CCR
	wseq.waddr   = `QUADSPI_CCR;
	//wseq.wdata   = 32'h00003521;  //  
	//wseq.wdata   = 32'h00003F21;  //   instruction and address on 4 lines
	wseq.wdata   = 32'h00003D21;  //   instruction and address on 4 lines
	//wseq.wdata   = 32'h00002F21;  //   instruction and address on 4 lines----------- 3 byte address
	 
    wseq.start(env.axi_agnt.sequencer);    
    
	// sending data to QUADSPI_AR
	wseq.waddr   = `QUADSPI_AR;
	//wseq.wdata   = 32'h00010000; 
	wseq.wdata   = 32'h0001F000; 	 
    wseq.start(env.axi_agnt.sequencer);
	
    wait(axi_master_tb_top_qspi.qspi_inst.qspi_ifc_rg_sr_tcf);
	
    // sending data to QUADSPI_FCR
	wseq.waddr   = `QUADSPI_FCR; 
	wseq.wdata   = 32'h0000001b; 	 
    wseq.start(env.axi_agnt.sequencer);
	   
    wait(!(axi_master_tb_top_qspi.flash_inst.flash.WIP));
    
	// sending data to QUADSPI_CCR
	wseq.waddr   = `QUADSPI_CCR;
	//wseq.wdata   = 32'h00000306;    	 
	wseq.wdata   = 32'h00000106;    	 
    wseq.start(env.axi_agnt.sequencer);

	wait(axi_master_tb_top_qspi.qspi_inst.qspi_ifc_rg_sr_tcf);
	   	
	// sending data to QUADSPI_FCR
	wseq.waddr   = `QUADSPI_FCR; 
	wseq.wdata   = 32'h0000001b; 	 
    wseq.start(env.axi_agnt.sequencer);

    wait(!(axi_master_tb_top_qspi.flash_inst.flash.WIP)); 
	    
	// sending data to QUADSPI_DLR
	wseq.waddr   = `QUADSPI_DLR; 
	wseq.wdata   = 32'h0000000F; // 16 bytes	 
    wseq.start(env.axi_agnt.sequencer);		
		
	// sending data to QUADSPI_CCR
	wseq.waddr   = `QUADSPI_CCR;
	//wseq.wdata   = 32'h01003512;    
	//wseq.wdata   = 32'h03003D34;    //write data on 4 lines
	//wseq.wdata   = 32'h03003F34;    //address instruction and data on 4 lines
	//wseq.wdata   = 32'h0303FF34;    //address instruction AB Mode =11 and data on 4 lines
	//wseq.wdata   = 32'h03003F34;    //address instruction AB Mode =11 and data on 4 lines
	//wseq.wdata   = 32'h03003F3E;    //address instruction AB Mode =11 and data on 4 lines
	//wseq.wdata   = 32'h03003F12;    //address instruction AB Mode =11 and data on 4 lines
	//wseq.wdata   = 32'h07003F12;    //address instruction AB Mode =11 and data on 4 lines
	wseq.wdata   = 32'h07003D12;    //address instruction AB Mode =11 and data on 4 lines
	//wseq.wdata   = 32'h03003F3E;    //address instruction AB Mode =11 and data on 4 lines
	//wseq.wdata   = 32'h03002F32;    //address instruction AB Mode =11 and data on 4 lines -------------3 byte address 
	//wseq.wdata   = 32'h03002F34;    //address instruction AB Mode =11 and data on 4 lines -------------3 byte address 
	 
    wseq.start(env.axi_agnt.sequencer);			
	   
	// sending data to QUADSPI_AR
	wseq.waddr   = `QUADSPI_AR;   
    //wseq.wdata   = 32'h00010000;
    wseq.wdata   = 32'h0001F000;	 
    wseq.start(env.axi_agnt.sequencer);		
	    
	// sending data to QUADSPI_DR_1
	wseq.waddr   = `QUADSPI_DR; 
    wseq.wdata   = 32'hff00ff12;   	 
    wseq.start(env.axi_agnt.sequencer);	

	// sending data to QUADSPI_DR_2
	wseq.waddr   = `QUADSPI_DR;
	wseq.wdata   = 32'hff00ff34; 
    wseq.start(env.axi_agnt.sequencer);
	   
	// sending data to QUADSPI_DR_3
	wseq.waddr   = `QUADSPI_DR;
	wseq.wdata   = 32'hff00ff00;   
    wseq.start(env.axi_agnt.sequencer);
    
    // sending data to QUADSPI_DR_4
	wseq.waddr   = `QUADSPI_DR;
	wseq.wdata   = 32'h11223344;    
    wseq.start(env.axi_agnt.sequencer);	   
	
    wait(axi_master_tb_top_qspi.qspi_inst.qspi_ifc_rg_sr_tcf);
		  
	// sending data to QUADSPI_FCR
	wseq.waddr   = `QUADSPI_FCR; 
	wseq.wdata   = 32'h0000001b; 
    wseq.start(env.axi_agnt.sequencer);			  

	wait(!(axi_master_tb_top_qspi.flash_inst.flash.WIP));
	   		
	// sending data to QUADSPI_DLR
	wseq.waddr   = `QUADSPI_DLR; 
	wseq.wdata   = 32'h0000000F; // 16 bytes
    wseq.start(env.axi_agnt.sequencer);

	// sending data to QUADSPI_CCR
	wseq.waddr   = `QUADSPI_CCR;
	// wseq.wdata   = 32'h0500750b;    
	//wseq.wdata   =      32'h05203513;  // Normal read operation - 8 dummy cycles
	//wseq.wdata   =      32'h07203513;  // quad Normal read operation - 8 dummy cycles
	//wseq.wdata   =      32'h07203DEC;  // address on 4 lines and data on 4 lines
	//wseq.wdata   =      32'h07203FEC;  // instruction address and data on 4 lines ----------------
	//wseq.wdata   =      32'h0723EFEC;  // instruction address and data on 4 lines  ad mode also included --- 3 byte address
	//wseq.wdata   =      32'h0718FFEC;  // instruction address and data on 4 lines  ad mode also included --- 4 byte address and 6dummy cycles
	//wseq.wdata   =      32'h0718FFEC;  // instruction address and data on 4 lines  ad mode also included --- 4 byte address and 6dummy cycles
	wseq.wdata   =      32'h07183DEC;  // instruction address and data on 4 lines  ad mode also included --- 4 byte address and 6dummy cycles
    // wseq.wdata   =   32'h0540750b;  // Added 16 dummy cycles 
	//wseq.wdata   =       32'h0540750c;  // Added 16 dummy cycles 0c for 4 byte address mode
	// wseq.wdata   =       32'h0520750c;  // Added 8 dummy cycles 0c for 4 byte address mode
    wseq.start(env.axi_agnt.sequencer);	
	     
	// sending data to QUADSPI_AR
	wseq.waddr   = `QUADSPI_AR;  
	//wseq.wdata   = 32'h00010000;
	wseq.wdata   = 32'h0001F000;
    wseq.start(env.axi_agnt.sequencer);			

    wait(axi_master_tb_top_qspi.qspi_inst.qspi_ifc_rg_sr_tcf);     
    repeat(4) begin	 
    // Reading data from QUADSPI_DR
	#1000000;
	rseq.raddr = `QUADSPI_DR;
    rseq.start(env.axi_agnt.sequencer);    
	end
//  wait(axi_master_tb_top_qspi.qspi_inst.qspi_ifc_rg_sr_tcf);
	    		
	// sending data to QUADSPI_FCR
	wseq.waddr   = `QUADSPI_FCR; 
	wseq.wdata   = 32'h0000001b; 
    wseq.start(env.axi_agnt.sequencer);    
		
	$display("writeReadStatusRegister command test case completed");
	
	phase.drop_objection(this);
	
    //set a drain-time for the environment if desired
	phase.phase_done.set_drain_time(this, 100);
  endtask : run_phase

endclass : writeReadStatusRegister
