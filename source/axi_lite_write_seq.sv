  
//class axi_lite_write_seq extends axi_lite_sequence#(axi_lite_seq_item);
class axi_lite_write_seq extends uvm_sequence#(axi_lite_seq_item);

 // AXI_transfer m_trx;
  
    `uvm_object_utils(axi_lite_write_seq)
  
  //--------------------------------------- 
  //Constructor
  //---------------------------------------
  function new(string name = "axi_lite_write_seq");
    super.new(name);
  endfunction
  
  bit [31:0] waddr;
  bit  addrv_i;  
  bit [128:0] wdata;
  bit  datav_i;
  
  virtual task body();
    
    req = axi_lite_seq_item::type_id::create("req");
    wait_for_grant();
    req.randomize();
   
    //req.write_start_i     = 1'b1;/ 
    req.write_addr_i      = this.waddr;
    req.write_addrv_i     = this.addrv_i;
    //req.write_addrv_i     = 1'b1;
    req.write_data_i      = this.wdata;
    //req.write_datav_i     = 1'b1;
    req.write_datav_i     = this.datav_i;
    req.write_strb_i      = 4'b1111;
    //req.read_start_i      = 1'b0;
    
    send_request(req);
    wait_for_item_done();
    
  endtask : body
  
endclass : axi_lite_write_seq