
class axi_lite_read_test extends axi_lite_base_test;

  `uvm_component_utils(axi_lite_read_test)
  
  //---------------------------------------
  // sequence instance 
  //--------------------------------------- 
  //wr_rd_sequence seq;
    
  axi_lite_read_seq rseq;

  //---------------------------------------
  // constructor
  //---------------------------------------
  function new(string name = "axi_lite_read_test",uvm_component parent=null);
    super.new(name,parent);
  endfunction : new

  //---------------------------------------
  // build_phase
  //---------------------------------------
  virtual function void build_phase(uvm_phase phase);
    super.build_phase(phase);

    // Create the sequence
    rseq = axi_lite_read_seq::type_id::create("rseq");
  endfunction : build_phase
  
  //---------------------------------------
  // run_phase - starting the test
  //---------------------------------------
    task run_phase(uvm_phase phase);
    
    phase.raise_objection(this);
    rseq.raddr     = 32'h0;
    
    /* for(int i=0;i<5;i++) begin
      rseq.raddr = rseq.raddr + 4;
      rseq.start(env.axi_agnt.sequencer);
    end */
	
    rseq.start(env.axi_agnt.sequencer);
    
    /* repeat(5) begin      
      rseq.raddr = rseq.raddr + 4;
      rseq.start(env.axi_agnt.sequencer);      
    end */
    phase.drop_objection(this);
    
    //set a drain-time for the environment if desired
    phase.phase_done.set_drain_time(this, 500);
      
  endtask : run_phase

endclass : axi_lite_read_test