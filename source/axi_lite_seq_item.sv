

class axi_lite_seq_item extends uvm_sequence_item;
  //---------------------------------------
  //data and control fields
  //---------------------------------------
  
  // write Interface Signals
  bit [31:0]write_addr_i ; 
  bit       write_addrv_i ; 
  rand bit [31:0]write_data_i ; 
  bit       write_datav_i ;
  bit [3:0] write_strb_i;  
  bit [1:0] write_bresp_o; 
  bit       write_bvalid_o;
  
  // read Interface Signals
  
  bit       read_start_i;
  bit [31:0]read_addr_i;
  bit       read_af_i;
  bit [31:0]read_data_o;
  bit       read_datav_o;    
  
  `uvm_object_utils(axi_lite_seq_item)
  
  //---------------------------------------
  //Constructor
  //---------------------------------------
  
  function new(string name = "axi_lite_seq_item");
    super.new(name);
  endfunction
  
  //---------------------------------------
  //constaint, to generate any one among write and read
  //---------------------------------------
 // constraint wr_rd_c{ write_start_i != read_start_i;};

endclass : axi_lite_seq_item