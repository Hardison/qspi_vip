
//class axi_lite_read_seq extends axi_lite_sequence#(axi_lite_seq_item);
class axi_lite_read_seq extends uvm_sequence#(axi_lite_seq_item);

 // AXI_transfer m_trx;
  
    `uvm_object_utils(axi_lite_read_seq)
  
  //--------------------------------------- 
  //Constructor
  //---------------------------------------
  function new(string name = "axi_lite_read_seq");
    super.new(name);
  endfunction
  
  bit [31:0] raddr;
  
  virtual task body();
    req = axi_lite_seq_item::type_id::create("req");
    wait_for_grant();
    req.randomize();
    
    req.read_start_i     = 1'b1;
    req.read_addr_i      = this.raddr;
    req.read_af_i        = 1'b0;   
    //req.write_start_i    = 1'b0;
                        
    send_request(req);
    wait_for_item_done();
    
  endtask : body
  
endclass : axi_lite_read_seq