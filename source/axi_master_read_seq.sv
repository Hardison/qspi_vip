


class axi_master_read_seq extends axi_sequence;

 // AXI_transfer m_trx;
  
    `uvm_object_utils(axi_master_read_seq)
  
  //--------------------------------------- 
  //Constructor
  //---------------------------------------
  function new(string name = "axi_master_read_seq");
    super.new(name);
  endfunction
  
  bit [31:0] raddr;
  bit [3:0] length;
  bit [3:0] arid;
  
  virtual task body();
    req = axi_seq_item::type_id::create("req");
    wait_for_grant();
    req.randomize();
    
    req.read_start_i     = 1'b1;
    //req.read_arid_i      = 4'b0001;
    req.read_arid_i      = this.arid;
    req.read_addr_i      = this.raddr;
    req.read_length_i    = this.length;
    req.read_size_i      = 3'b010;
    req.read_burst_i     = 2'b00;
    req.read_lock_i      = 2'b00;
    req.read_af_i        = 1'b0;   
    req.write_start_i    = 1'b0;
                        
    send_request(req);
    wait_for_item_done();
    
  endtask : body
  
endclass : axi_master_read_seq