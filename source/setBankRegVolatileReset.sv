// Able to write on the Bank reg Register and read back .Also performed the Soft reset then read back to check reset worked or not .
//Comment :Getting data on SO line, but Qspi_reg_phase is in Dummy phase at that instant.(it should be in data read phase i.e 4)

`include "defines.sv"

class setBankRegVolatileReset extends axi_lite_base_test;
    
  `uvm_component_utils(setBankRegVolatileReset)
  
  //---------------------------------------
  // sequence instance 
  ////-------------------------------------
  
  axi_lite_write_seq wseq;
  axi_lite_read_seq rseq;

  //---------------------------------------
  // constructor
  //---------------------------------------
  function new(string name = "setBankRegVolatileReset",uvm_component parent=null);
    super.new(name,parent);
  endfunction : new

  //---------------------------------------
  // build_phase
  //---------------------------------------
  virtual function void build_phase(uvm_phase phase);    
    super.build_phase(phase);

    // Create the sequence
    
    wseq = axi_lite_write_seq::type_id::create("wseq");
    rseq = axi_lite_read_seq::type_id::create("rseq");
		
  endfunction : build_phase
  
  //---------------------------------------
  // run_phase - starting the test
  //---------------------------------------
  task run_phase(uvm_phase phase);
    
    phase.raise_objection(this);    
	wseq.waddr      = 0; 
	wseq.wdata      = 32'h00000000;
	wseq.addrv_i    = 1'b0;
	wseq.datav_i    = 1'b0;
	
	#100000;
	//wseq.waddr      = 32'1; 
	//wseq.wdata      = 32'0;
	wseq.addrv_i    = 1'b1;
	wseq.datav_i    = 1'b1; 
	    
	// sending address and data to QUADSPI_CR
	wseq.waddr   = `QUADSPI_CR; 
	wseq.wdata   = 32'h031f0f01; // Enable QSPI 25 MHz 
    wseq.start(env.axi_agnt.sequencer);
	
    // sending data to QUADSPI_DCR
	wseq.waddr   = `QUADSPI_DCR; 
	wseq.wdata   = 32'h001b0001; 
    wseq.start(env.axi_agnt.sequencer);
         
    wait(axi_master_tb_top_qspi.flash_inst.flash.Chip_EN);
	
	//-------------configuring Set Bank register volatile-------------
	// sending data to QUADSPI_DLR
	wseq.waddr   = `QUADSPI_DLR; 
	wseq.wdata   = 32'h00000001; // 1 bytes
    wseq.start(env.axi_agnt.sequencer);
	
	// sending data to QUADSPI_CCR
	wseq.waddr   = `QUADSPI_CCR;
	wseq.wdata   = 32'h01000117;  // Set Bank register register volatile
	//wseq.wdata   = 32'h010001C5;  // Set Bank register register volatile
    wseq.start(env.axi_agnt.sequencer);	
	
	// sending data to QUADSPI_DR_1
	wseq.waddr   = `QUADSPI_DR; 
    //wseq.wdata   = 32'h30f0f0f0;  
    wseq.wdata   = 32'h81000000;  
    wseq.start(env.axi_agnt.sequencer);	

	
	wait(axi_master_tb_top_qspi.qspi_inst.qspi_ifc_rg_sr_tcf);
    // sending data to QUADSPI_FCR
	wseq.waddr   = `QUADSPI_FCR; 
	wseq.wdata   = 32'h0000001b;  
    wseq.start(env.axi_agnt.sequencer);	    
	
	wait(!(axi_master_tb_top_qspi.flash_inst.flash.WIP));
	
	//----------read Bank reg-------------
	
	wseq.waddr   = `QUADSPI_DLR; 
	wseq.wdata   = 32'h00000000; // 1 bytes
    wseq.start(env.axi_agnt.sequencer);

	// sending data to QUADSPI_CCR
	wseq.waddr   = `QUADSPI_CCR;
	wseq.wdata   = 32'h05180116;    // Read Bank add register 6 dummy cycles-- 
	//wseq.wdata   = 32'h051001C8;    // Read Bank add register 
    wseq.start(env.axi_agnt.sequencer);		 			
         
    // Reading data from QUADSPI_DR
	rseq.raddr = `QUADSPI_DR; //Getting data on SO line but it is in dummy phase.
    rseq.start(env.axi_agnt.sequencer);   
	
	 wait(axi_master_tb_top_qspi.qspi_inst.qspi_ifc_rg_sr_tcf);
    // sending data to QUADSPI_FCR
	wseq.waddr   = `QUADSPI_FCR; 
	wseq.wdata   = 32'h0000001b;  
    wseq.start(env.axi_agnt.sequencer);	
	
	wait(!(axi_master_tb_top_qspi.flash_inst.flash.WIP));
	
	//---------------Reset_enable-----------------------
		// sending data to QUADSPI_CCR
	wseq.waddr   = `QUADSPI_CCR;
	wseq.wdata   = 32'h00000166;  //  Reset_Enable
    wseq.start(env.axi_agnt.sequencer);
    
   wait(axi_master_tb_top_qspi.qspi_inst.qspi_ifc_rg_sr_tcf);
    // sending data to QUADSPI_FCR
	wseq.waddr   = `QUADSPI_FCR; 
	wseq.wdata   = 32'h0000001b;  
    wseq.start(env.axi_agnt.sequencer);	
	
	wait(!(axi_master_tb_top_qspi.flash_inst.flash.WIP));
    
    // sending data to QUADSPI_CCR
	wseq.waddr   = `QUADSPI_CCR;
	wseq.wdata   = 32'h00000199;  //  Soft Reset
    wseq.start(env.axi_agnt.sequencer);
	
    wait(axi_master_tb_top_qspi.qspi_inst.qspi_sr_tcf);
    // sending data to QUADSPI_FCR
	wseq.waddr   = `QUADSPI_FCR; 
	wseq.wdata   = 32'h0000001b; 
    wseq.start(env.axi_agnt.sequencer);	
	#100000000;
	//----------read Bank reg-------------
	
	wseq.waddr   = `QUADSPI_DLR; 
	wseq.wdata   = 32'h00000000; // 1 bytes
    wseq.start(env.axi_agnt.sequencer);

	// sending data to QUADSPI_CCR
	wseq.waddr   = `QUADSPI_CCR;
	wseq.wdata   = 32'h05180116;    // Read Bank add register 6 dummy cycles
	//wseq.wdata   = 32'h05100116;    // Read bank Address register 4 dummy cycles
	//wseq.wdata   = 32'h05200116;    // Read bank Address register 8 dummy cycles
	//wseq.wdata   = 32'h052001C8;    // Read bank Address register 
    wseq.start(env.axi_agnt.sequencer);		 			
         
    // Reading data from QUADSPI_DR
	rseq.raddr = `QUADSPI_DR;
    rseq.start(env.axi_agnt.sequencer);   
	
	 wait(axi_master_tb_top_qspi.qspi_inst.qspi_ifc_rg_sr_tcf);
    // sending data to QUADSPI_FCR
	wseq.waddr   = `QUADSPI_FCR; 
	wseq.wdata   = 32'h0000001b;  
    wseq.start(env.axi_agnt.sequencer);	
	
	wait(!(axi_master_tb_top_qspi.flash_inst.flash.WIP));
	
    phase.drop_objection(this);
	
    //set a drain-time for the environment if desired
    phase.phase_done.set_drain_time(this, 100);
    
  endtask : run_phase

endclass : setBankRegVolatileReset
