
`include "defines.sv"
class PP_12_4BYTS extends axi_lite_base_test;
    
  `uvm_component_utils(PP_12_4BYTS)
  
  //---------------------------------------
  // sequence instance 
  ////-------------------------------------
  
  axi_lite_write_seq wseq;
  axi_lite_read_seq rseq;

  //---------------------------------------
  // constructor
  //---------------------------------------
  function new(string name = "PP_12_4BYTS",uvm_component parent=null);
    super.new(name,parent);
  endfunction : new

  //---------------------------------------
  // build_phase
  //---------------------------------------
  virtual function void build_phase(uvm_phase phase);    
    super.build_phase(phase);

    // Create the sequence
    
    wseq = axi_lite_write_seq::type_id::create("wseq");
    rseq = axi_lite_read_seq::type_id::create("rseq");
		
  endfunction : build_phase
  
  //---------------------------------------
  // run_phase - starting the test
  //---------------------------------------
  task run_phase(uvm_phase phase);
    
    phase.raise_objection(this);  
	
	wseq.waddr      = 0; 
	wseq.wdata      = 32'h00000000;
	wseq.addrv_i    = 1'b0;
	wseq.datav_i    = 1'b0;
	
	#100000;
	//wseq.waddr      = 32'1; 
	//wseq.wdata      = 32'0;
	wseq.addrv_i    = 1'b1;
	wseq.datav_i    = 1'b1;
	    
	// sending address and data to QUADSPI_CR
	wseq.waddr   = `QUADSPI_CR; 
	wseq.wdata   = 32'h031f0f01; // Enable Flash 25 MHz
    wseq.start(env.axi_agnt.sequencer);
	
    // sending data to QUADSPI_DCR
	wseq.waddr   = `QUADSPI_DCR; 
	wseq.wdata   = 32'h001b0001; 
    wseq.start(env.axi_agnt.sequencer);
         
    wait(axi_master_tb_top_qspi.flash_inst.flash.Chip_EN);
		
    // sending address and data to QUADSPI_CCR
	wseq.waddr   = `QUADSPI_CCR; 
	wseq.wdata   = 32'h00003106;  
    wseq.start(env.axi_agnt.sequencer);
		
 	wait(axi_master_tb_top_qspi.qspi_inst.qspi_ifc_rg_sr_tcf);
    // sending data to QUADSPI_FCR
	wseq.waddr   = `QUADSPI_FCR; 
	wseq.wdata   = 32'h0000001b;
    wseq.start(env.axi_agnt.sequencer);
	
    // sending data to QUADSPI_CCR
	wseq.waddr   = `QUADSPI_CCR;
	wseq.wdata   = 32'h00003521;  // 
    wseq.start(env.axi_agnt.sequencer);
	
	// sending data to QUADSPI_AR
	wseq.waddr   = `QUADSPI_AR; 
	wseq.wdata   = 32'h00030000; 
    wseq.start(env.axi_agnt.sequencer);
	
 	wait(axi_master_tb_top_qspi.qspi_inst.qspi_ifc_rg_sr_tcf);
    // sending data to QUADSPI_FCR
	wseq.waddr   = `QUADSPI_FCR; 
	wseq.wdata   = 32'h0000001b;	
    wseq.start(env.axi_agnt.sequencer);
	
    wait(!(axi_master_tb_top_qspi.flash_inst.flash.WIP));
	
	// sending data to QUADSPI_CCR
	wseq.waddr   = `QUADSPI_CCR;
	wseq.wdata   = 32'h00000106;  // 
    wseq.start(env.axi_agnt.sequencer);
	
	wait(axi_master_tb_top_qspi.qspi_inst.qspi_ifc_rg_sr_tcf);
	   	
	// sending data to QUADSPI_FCR
	wseq.waddr   = `QUADSPI_FCR; 
	wseq.wdata   = 32'h0000001b; 
    wseq.start(env.axi_agnt.sequencer);
	
    wait(!(axi_master_tb_top_qspi.flash_inst.flash.WIP)); 
	    
	// sending data to QUADSPI_DLR
	wseq.waddr   = `QUADSPI_DLR; 
	wseq.wdata   =  32'h0000000F; 
    wseq.start(env.axi_agnt.sequencer);

	// sending data to QUADSPI_AR
	wseq.waddr   = `QUADSPI_AR;   
    wseq.wdata   = 32'h00030000;
    wseq.start(env.axi_agnt.sequencer);
	
	// sending data to QUADSPI_CCR
	wseq.waddr   = `QUADSPI_CCR;
	wseq.wdata   = 32'h01003512;  	
    wseq.start(env.axi_agnt.sequencer);

	// sending data to QUADSPI_DR_1
	wseq.waddr   = `QUADSPI_DR; 
    wseq.wdata   = 32'hff0000FE;  
    wseq.start(env.axi_agnt.sequencer);	

	// sending data to QUADSPI_DR_2
	wseq.waddr   = `QUADSPI_DR;
	wseq.wdata   = 32'h12345678;   
    wseq.start(env.axi_agnt.sequencer);
	   
	// sending data to QUADSPI_DR_3
	wseq.waddr   = `QUADSPI_DR;
	wseq.wdata   = 32'h45671234;  
    wseq.start(env.axi_agnt.sequencer);
    
    // sending data to QUADSPI_DR_4
	wseq.waddr   = `QUADSPI_DR;
	wseq.wdata   = 32'h87654321;   
    wseq.start(env.axi_agnt.sequencer);	  
	
    wait(axi_master_tb_top_qspi.qspi_inst.qspi_ifc_rg_sr_tcf);	
	
	// sending data to QUADSPI_FCR
	wseq.waddr   = `QUADSPI_FCR; 
	wseq.wdata   = 32'h0000001b; 
    wseq.start(env.axi_agnt.sequencer);			  

	wait(!(axi_master_tb_top_qspi.flash_inst.flash.WIP));		

	// sending address and data to QUADSPI_DLR
	wseq.waddr   = `QUADSPI_DLR; 
	wseq.wdata   = 32'h0000000F; 
    wseq.start(env.axi_agnt.sequencer);
  
    // sending address and data to QUADSPI_CCR
	wseq.waddr   = `QUADSPI_CCR; 
	wseq.wdata   = 32'h05003513;  
	//wseq.wdata   = 32'h85003513;      // ddr
    wseq.start(env.axi_agnt.sequencer);
	
	// sending data to QUADSPI_AR
	wseq.waddr   = `QUADSPI_AR; 
	wseq.wdata   = 32'h00030000; 
    wseq.start(env.axi_agnt.sequencer);	
	
	//#1000000;
	repeat(4) begin
	// Reading data from QUADSPI_DR 1
	#10000000;
	//#10000000;
	rseq.raddr = `QUADSPI_DR;
    rseq.start(env.axi_agnt.sequencer);
	end

 	wait(axi_master_tb_top_qspi.qspi_inst.qspi_ifc_rg_sr_tcf);  //qspi_lite signal	

	// sending data to QUADSPI_FCR
	wseq.waddr   = `QUADSPI_FCR; 
	wseq.wdata   = 32'h0000001b; 
    wseq.start(env.axi_agnt.sequencer);	 

    $display("Page program test case completed");
	#10000000;
    phase.drop_objection(this);
	
    //set a drain-time for the environment if desired
    phase.phase_done.set_drain_time(this, 100);
    
  endtask : run_phase

endclass : PP_12_4BYTS