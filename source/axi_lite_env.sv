
`include "axi_lite_agent.sv"
`include "axi_lite_scoreboard.sv"

class axi_lite_env extends uvm_env;
  
  //---------------------------------------
  // agent and scoreboard instance
  //---------------------------------------
  axi_lite_agent      axi_agnt;
  axi_lite_scoreboard axi_scb;
  
  `uvm_component_utils(axi_lite_env)
  
  //--------------------------------------- 
  // constructor
  //---------------------------------------
  function new(string name, uvm_component parent);
    super.new(name, parent);
  endfunction : new

  //---------------------------------------
  // build_phase - crate the components
  //---------------------------------------
  function void build_phase(uvm_phase phase);
    super.build_phase(phase);

    axi_agnt = axi_lite_agent::type_id::create("axi_agnt", this);
    axi_scb  = axi_lite_scoreboard::type_id::create("axi_scb", this);
  endfunction : build_phase
  
  //---------------------------------------
  // connect_phase - connecting monitor and scoreboard port
  //---------------------------------------
  function void connect_phase(uvm_phase phase);
    axi_agnt.monitor.item_collected_port.connect(axi_scb.item_collected_export);
  endfunction : connect_phase

endclass : axi_lite_env