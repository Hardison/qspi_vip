//Description: After freeze, write operations has to be ignored . Freeze feature is failed able to do wr txns after set freeze to 1.

`include "defines.sv"

class Freeze_InformationRow_spi extends axi_lite_base_test;
    
  `uvm_component_utils(Freeze_InformationRow_spi)
  
  //---------------------------------------
  // sequence instance 
  ////-------------------------------------
  
  axi_lite_write_seq wseq;
  axi_lite_read_seq rseq;

  //---------------------------------------
  // constructor
  //---------------------------------------
  function new(string name = "Freeze_InformationRow_spi",uvm_component parent=null);
    super.new(name,parent);
  endfunction : new

  //---------------------------------------
  // build_phase
  //---------------------------------------
  virtual function void build_phase(uvm_phase phase);    
    super.build_phase(phase);

    // Create the sequence
    
    wseq = axi_lite_write_seq::type_id::create("wseq");
    rseq = axi_lite_read_seq::type_id::create("rseq");
		
  endfunction : build_phase
  
  //---------------------------------------
  // run_phase - starting the test
  //---------------------------------------
  task run_phase(uvm_phase phase);
    
    phase.raise_objection(this);    
	wseq.waddr      = 0; 
	wseq.wdata      = 32'h00000000;
	wseq.addrv_i    = 1'b0;
	wseq.datav_i    = 1'b0;
	
	#100000;
	//wseq.waddr      = 32'1; 
	//wseq.wdata      = 32'0;
	wseq.addrv_i    = 1'b1;
	wseq.datav_i    = 1'b1; 
	    
	// sending address and data to QUADSPI_CR
	wseq.waddr   = `QUADSPI_CR; 
	wseq.wdata   = 32'h031f0f01; // Enable QSPI 25 MHz
    wseq.start(env.axi_agnt.sequencer);
	
    // sending data to QUADSPI_DCR
	wseq.waddr   = `QUADSPI_DCR; 
	wseq.wdata   = 32'h001b0001;  
    wseq.start(env.axi_agnt.sequencer);
      
    wait(axi_master_tb_top_qspi.flash_inst.flash.Chip_EN);
	
/* 	//sending data to QUADSPI_CCR
	wseq.waddr   = `QUADSPI_CCR;
	wseq.wdata   = 32'h00000135;  //  Enable Quad mode 
    wseq.start(env.axi_agnt.sequencer);
	
	wait(axi_master_tb_top_qspi.qspi_inst.qspi_ifc_rg_sr_tcf);
	
    // sending data to QUADSPI_FCR
	wseq.waddr   = `QUADSPI_FCR; 
	wseq.wdata   = 32'h0000001b;  
    wseq.start(env.axi_agnt.sequencer);	 */	
	
	// sending data to QUADSPI_CCR
	wseq.waddr   = `QUADSPI_CCR;
	wseq.wdata   = 32'h00000106;  //  WEL 
    wseq.start(env.axi_agnt.sequencer);
    
	wait(axi_master_tb_top_qspi.qspi_inst.qspi_ifc_rg_sr_tcf);
	
    // sending data to QUADSPI_FCR
	wseq.waddr   = `QUADSPI_FCR; 
	wseq.wdata   = 32'h0000001b; 
 
    wseq.start(env.axi_agnt.sequencer);	
	
	//sending data to QUADSPI_CCR
	wseq.waddr   = `QUADSPI_CCR;
	wseq.wdata   = 32'h00000191;  //freeze 
    wseq.start(env.axi_agnt.sequencer);
	
	wait(axi_master_tb_top_qspi.qspi_inst.qspi_ifc_rg_sr_tcf);
	
    // sending data to QUADSPI_FCR
	wseq.waddr   = `QUADSPI_FCR; 
	wseq.wdata   = 32'h0000001b;  
    wseq.start(env.axi_agnt.sequencer);	
	
	wait(!(axi_master_tb_top_qspi.flash_inst.flash.WIP));
	
	
	
	
	
/* 		//---------------------------------------
	// Reading from Flash Started
	//---------------------------------------		
			
	// sending data to QUADSPI_DLR
	wseq.waddr   = `QUADSPI_DLR; 
	wseq.wdata   = 32'h00000000; //  Reading 1 bytes data	 
    wseq.start(env.axi_agnt.sequencer);

	// sending data to QUADSPI_CCR
	wseq.waddr   = `QUADSPI_CCR;
	//wseq.wdata   =  32'h05200105; // Reading flash status Register,We are getting default values even though we have written in the Flash Status Register. 
	//wseq.wdata   =  32'h07180305; // Reading flash status Register.6 dummy Quad mode
	wseq.wdata   =  32'h05000105; // Reading flash status Register.0 dummy Quad mode	 
    wseq.start(env.axi_agnt.sequencer);		  
	
    #10000000;	
    // Reading data from QUADSPI_DR
	rseq.raddr = `QUADSPI_DR;
    rseq.start(env.axi_agnt.sequencer);      
	
    wait(axi_master_tb_top_qspi.qspi_inst.qspi_ifc_rg_sr_tcf);
	    		
	// sending data to QUADSPI_FCR
	wseq.waddr   = `QUADSPI_FCR; 
	wseq.wdata   = 32'h0000001b; // Flag Clear Register	 
    wseq.start(env.axi_agnt.sequencer);

	 */
	//---------------------------------------
	// Reading from Flash Started
	//---------------------------------------	
	
	
	// sending data to QUADSPI_DLR
	wseq.waddr   = `QUADSPI_DLR; 
	wseq.wdata   = 32'h00000000; //  Reading 1 bytes data	 
    wseq.start(env.axi_agnt.sequencer);

	// sending data to QUADSPI_CCR
	wseq.waddr   = `QUADSPI_CCR;
	wseq.wdata   =  32'h050001A7; // Reading flash status Register.0 dummy Quad mode	 
    wseq.start(env.axi_agnt.sequencer);		  
	
    #10000000;	
    // Reading data from QUADSPI_DR
	rseq.raddr = `QUADSPI_DR;
    rseq.start(env.axi_agnt.sequencer);      
	
    wait(axi_master_tb_top_qspi.qspi_inst.qspi_ifc_rg_sr_tcf);
	    		
	// sending data to QUADSPI_FCR
	wseq.waddr   = `QUADSPI_FCR; 
	wseq.wdata   = 32'h0000001b; // Flag Clear Register	 
    wseq.start(env.axi_agnt.sequencer);
	
	
	
	
	wait(!(axi_master_tb_top_qspi.flash_inst.flash.WIP));	
	
	// sending data to QUADSPI_CCR
	wseq.waddr   = `QUADSPI_CCR;
	wseq.wdata   = 32'h00000106;  //  WEL 
    wseq.start(env.axi_agnt.sequencer);
    
    wait(axi_master_tb_top_qspi.qspi_inst.qspi_ifc_rg_sr_tcf);
    // sending data to QUADSPI_FCR
	wseq.waddr   = `QUADSPI_FCR; 
	wseq.wdata   = 32'h0000001b;  
    wseq.start(env.axi_agnt.sequencer);	
	
	// sending data to QUADSPI_DLR
	wseq.waddr   = `QUADSPI_DLR;
	wseq.wdata   = 32'h0000000F;  //  16 bytes 
    wseq.start(env.axi_agnt.sequencer);
	
	// sending data to QUADSPI_CCR
	wseq.waddr   = `QUADSPI_CCR;
	wseq.wdata   = 32'h01003562;  //  IR Program 
    wseq.start(env.axi_agnt.sequencer);	
	
	// sending data to QUADSPI_AR
	wseq.waddr   = `QUADSPI_AR;
	wseq.wdata   = 32'h00000000;  
    wseq.start(env.axi_agnt.sequencer);
	
	// sending data to QUADSPI_DR
	wseq.waddr   = `QUADSPI_DR;
    wseq.wdata   = 32'hff00ff10;  //   
    wseq.start(env.axi_agnt.sequencer);	

	// sending data to QUADSPI_DR
	wseq.waddr   = `QUADSPI_DR;
	wseq.wdata   = 32'hff00ff20;  //   
    wseq.start(env.axi_agnt.sequencer);
	   
	// sending data to QUADSPI_DR
	wseq.waddr   = `QUADSPI_DR;
	wseq.wdata   = 32'hff00ff30;  //   
    wseq.start(env.axi_agnt.sequencer);
    
    // sending data to QUADSPI_DR
	wseq.waddr   = `QUADSPI_DR;
	wseq.wdata   = 32'hff00ff40;  //   
    wseq.start(env.axi_agnt.sequencer);	
	
	 wait(axi_master_tb_top_qspi.qspi_inst.qspi_ifc_rg_sr_tcf);
		  
	// sending data to QUADSPI_FCR
	wseq.waddr   = `QUADSPI_FCR; 
	wseq.wdata   = 32'h0000001b;  
    wseq.start(env.axi_agnt.sequencer);	

	wait(!(axi_master_tb_top_qspi.flash_inst.flash.WIP));
    
    $display("Information row program read command test case completed");	
	       
    phase.drop_objection(this);
	
    //set a drain-time for the environment if desired
    phase.phase_done.set_drain_time(this, 100);
    
  endtask : run_phase

endclass : Freeze_InformationRow_spi         

