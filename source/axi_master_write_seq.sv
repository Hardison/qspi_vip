
class axi_master_write_seq extends axi_sequence;

 // AXI_transfer m_trx;
  
    `uvm_object_utils(axi_master_write_seq)
  
  //--------------------------------------- 
  //Constructor
  //---------------------------------------
  function new(string name = "axi_master_write_seq");
    super.new(name);
  endfunction  
   
  bit [31:0] waddr;
  bit [3:0] length;
  bit [3:0] awid;
  //bit [3:0] wid; Commented as it is not there in New mkdummy.
	bit [63:0] wdata;
	bit wdata_v;  //change by nov 5th
  
  virtual task body();
    
    req = axi_seq_item::type_id::create("req");
    wait_for_grant();
    req.randomize();
   
    req.write_start_i     = 1'b1;
    //req.write_awid_i      = 4'b0001;    
    req.write_awid_i      = this.awid;
    req.write_addr_i      = this.waddr;
    req.write_length_i    = this.length;
	req.write_data_i      = this.wdata;
    req.write_size_i      = 3'b010;
    req.write_burst_i     = 2'b00;
    req.write_lock_i      = 2'b00;
    //req.write_wid_i       = 4'b0001;
    //req.write_wid_i       = this.wid; Commented as it is not there in New mkdummy.
    req.write_datav_i     = this.wdata_v;   //change by nov 5th
    //req.write_datav_i     = 1;
    req.write_strb_i      = 8'b11111111;
    req.read_start_i      = 1'b0;
    
    send_request(req);
    wait_for_item_done();
    
  endtask : body
  
endclass : axi_master_write_seq