# QSPI VIP

## Setup
```
$ export BSC_HOME=<bsc_source_path>
$ export UVM_HOME=<uvm-1.2 path>
```

## Design compile
```
$ cd sim
$ source design_build.sh
```

## Test bench compile
```
$ cd sim
$ make -f Makefile.questa compile
```

## Test simulation
```
$ cd sim
$ make -f Makefile.questa sim TEST=page_program_1
```
